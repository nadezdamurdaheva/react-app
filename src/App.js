import { Root } from "./Root.jsx";

function App() {
  return (
    <div>
      <Root />
    </div>
  );
}

export default App;
